defmodule PublicSuffix.RemoteFileFetcher do
  @moduledoc false

  def fetch_remote_file(url) when is_binary(url) do
    {:ok, _} = Application.ensure_all_started(:httpoison)

    url
    |> to_charlist()
    |> HTTPoison.get()
    |> case do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} -> {:ok, body}
      otherwise -> {:error, otherwise}
    end
  end
end
